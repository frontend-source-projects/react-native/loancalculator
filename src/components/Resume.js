import React from "react";
import {StyleSheet, View, Text} from "react-native";

export default function Resume(props) {
    const {request, porcent, monthSelected, result, message} = props;
    return (
        <View style = {styles.content}>
            {result && (
                <View style = {styles.boxResume}>
                    <Text style = {styles.title}>Resume</Text>
                    <Data title = "Requested amount:" value = {request} label = "$"></Data>
                    <Data title = "Porcent(%):" value = {porcent} label = "%"></Data>
                    <Data title = "Mounths:" value = {monthSelected} label = "mounths"></Data>
                    <Data title = "Mounthly Pay" value = {result.mounthlyFee} label = "$"></Data>
                    <Data title = "Total" value = {result.totalFee} label = "$"></Data>
                </View>
            )}

            <View>
                <Text style = {styles.error}>{message}</Text>
            </View>
        </View>
    );
}

function Data(props) {
    const {title, value, label} = props;
    return (
        <View style = {styles.value}>
            <Text>{title}</Text>
            <Text>{value} {label}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    content: {
        marginHorizontal: 40
    },
    boxResume: {
        padding: 30
    },
    title: {
        fontSize: 20,
        textAlign: "center",
        fontWeight: "bold",
        marginBottom: 20
    },
    value: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    error: {
        textAlign: "center",
        color: "#F00",
        fontWeight: "bold",
        fontSize: 20
    }
});

