import React from "react";
import {StyleSheet, TextInput, View} from "react-native";
import RNPickerSelect from 'react-native-picker-select';
import colors from "../utils/colors";

export default function Form(props) {
    const {setRequest, setPorcent, setMonthSelected} = props;

    return (
        <View style = {styles.viewForm}>
            <View style = {styles.viewFormInput}>
                <TextInput 
                    placeholder = "Mounth to Request" 
                    keyboardType = "numeric" 
                    style = {styles.inputText}
                    onChange = {(e) => setRequest(e.nativeEvent.text)}
                >                    
                </TextInput>
                <TextInput 
                    placeholder = "Porcent" 
                    keyboardType = "numeric" 
                    style = {[styles.inputText, styles.inputPorcent]}
                    onChange = {(e) => setPorcent(e.nativeEvent.text)}
                >
                </TextInput>
            </View>
            <RNPickerSelect
            style = {picketSelectStyles}
            onValueChange={(value) => setMonthSelected(value)}
            items={[
                { label: '3 Months', value: 3 },
                { label: '6 Months', value: 6 },
                { label: '9 Months', value: 9 },
                { label: '12 Months', value: 12 },
            ]}
            placeholder= {{
                label: "Select months",
                value: null
            }}
        />
        </View>
    );
}

const styles = StyleSheet.create({
    viewForm: {
        position: "absolute",
        bottom: 0,
        width: "85%",
        paddingHorizontal: 50,
        backgroundColor: colors.PRIMARY_COLOR_DARK,
        borderRadius: 30,
        height: 180,
        justifyContent: "center"
    },
    viewFormInput:{
        flexDirection: "row"
    },
    inputText: {
        height: 50,
        backgroundColor: "#FFFFFF",
        borderWidth: 1,
        borderColor: colors.PRIMARY_COLOR_BRIGTH,
        borderRadius: 5,
        width: "60%",
        marginRight: 5,
        marginLeft: -5 ,
        marginBottom: 10,
        color: "#000000",
        paddingHorizontal: 20
    },
    inputPorcent: {
        width: "40%",
        marginLeft: 5
    }
  });

const picketSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 10,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: "grey",
        borderRadius: 4,
        color: "black",
        paddingRight: 30,
        backgroundColor: "#FFFFFF",
        marginLeft: -5,
        marginRight: -5
    },
    inputAndroid: {
        fontSize: 10,
        paddingVertical: 8,
        paddingHorizontal: 10,
        borderWidth: 0.5,
        borderColor: "grey",
        borderRadius: 8,
        color: "black",
        paddingRight: 30,
        backgroundColor: "#FFFFFF"
    }

});