import React from "react";
import {StyleSheet, View, Text, TouchableOpacity} from "react-native";
import colors from "../utils/colors";

export default function Footer(props) {
    const {onSubmitCalculate} = props;

    return (
        <View style = {styles.viewFooter}>
            <TouchableOpacity style = {styles.touchableButton} onPress = {onSubmitCalculate}>
                <Text style = {styles.textButton}>Process</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    viewFooter: {
        position: "absolute",
        bottom: 0,
        width: "100%",
        backgroundColor: colors.PRIMARY_COLOR_BRIGTH,
        height: 100,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        alignItems: "center",
        justifyContent: "center"
    },
    textButton: {
        fontWeight: "bold",
        fontSize: 18,
        color: "#FFFFFF"
    },
    touchableButton: {
        backgroundColor: colors.PRIMARY_COLOR_DARK,
        padding: 16,
        borderRadius: 20,
        width: "75%",
        alignItems: "center",
        justifyContent: "center"
    }
});

