import React, {useState, useEffect} from "react";
import {StyleSheet, View, Text, SafeAreaView, StatusBar, Button} from "react-native";
import colors from "./src/utils/colors";
import Form from "./src/components/Form";
import Footer from "./src/components/Footer";
import Resume from "./src/components/Resume";


export default  function App () {
const [request, setRequest] = useState(null);
const [porcent, setPorcent] = useState(null);
const [monthSelected, setMonthSelected] = useState(null);
const [result, setResult] = useState(null);
const [message, setMessage] = useState("");

useEffect(() => {
  const hasAllFields = request && porcent && monthSelected;
  hasAllFields ? onSubmitCalculate() :  reset();
}, [request, porcent, monthSelected])

const onSubmitCalculate = () => {
  reset();
  if(!request) {
    setMessage("Need request!");
  } else if (!porcent) {
    setMessage("Need porcent!");
  } else if (!monthSelected) {
    setMessage("Need mouth!");
  } else {
    const interes = (porcent / 100);
    const mounthlyFee = (request / ((1 - Math.pow(interes + 1, -monthSelected)) / interes)).toFixed(2);
    const totalFee = (mounthlyFee * monthSelected).toFixed(2);
    setResult({
      mounthlyFee: mounthlyFee,
      totalFee: totalFee
    });
  }

};

const reset = () => {
  setMessage("");
  setResult(null);
};

  return (
      <>
        <StatusBar barStyle = {"light-content"}></StatusBar>

        <SafeAreaView style={styles.safeArea}>
          <View style = {styles.background}></View>
          <Text style = {styles.titleApp}>Calculator</Text>
          <Form 
            setRequest = {setRequest} 
            setPorcent = {setPorcent} 
            setMonthSelected = {setMonthSelected}>
          </Form>
        </SafeAreaView>

        <Resume 
          request = {request} 
          porcent = {porcent} 
          monthSelected = {monthSelected}
          result = {result}
          message = {message}>            
          </Resume>

        <Footer onSubmitCalculate = {onSubmitCalculate}></Footer>
      </>
);
}

const styles = StyleSheet.create({
  safeArea: {
    height: 290,
    alignItems: "center"
  },
  titleApp: {
    fontSize: 25,
    fontWeight: "bold",
    color: "#FFFFFF",
    marginTop: 15
  },
  background: {
    backgroundColor: colors.PRIMARY_COLOR_BRIGTH,
    height: 200,
    width: "100%",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    position: "absolute",
    zIndex: -1
  }
});